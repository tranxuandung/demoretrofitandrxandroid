package com.vmodev.demoretrofit.module;

/**
 * Created by dungtx on 7/24/17.
 */

public class ItemSongResponse {
    private String dataCode;
    private String title;

    public ItemSongResponse(String dataCode, String title) {
        this.dataCode = dataCode;
        this.title = title;
    }

    public String getDataCode() {
        return dataCode;
    }

    public void setDataCode(String dataCode) {
        this.dataCode = dataCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
