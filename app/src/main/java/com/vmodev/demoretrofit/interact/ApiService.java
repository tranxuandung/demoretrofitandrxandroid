package com.vmodev.demoretrofit.interact;

import com.vmodev.demoretrofit.module.ItemSongSearch;

import java.util.List;
import io.reactivex.Observable;

import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by dungtx on 7/24/17.
 */

public interface ApiService {

    @GET("jOut.ashx")
    Observable<List<ItemSongSearch>> queryMusic(@Query("k") String musicName, @Query("h") String webSite, @Query("code") String code);
}
