package com.vmodev.demoretrofit.interact;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.vmodev.demoretrofit.module.ItemSongResponse;
import com.vmodev.demoretrofit.module.ItemSongSearch;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dungtx on 7/24/17.
 */

public class ApiConnector {
    private static ApiConnector instance = new ApiConnector();
    private ApiService service;

    public ApiConnector() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS);

        Retrofit.Builder retrofit = new Retrofit.Builder();
        retrofit.baseUrl("http://j.ginggong.com")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build());
        service = retrofit.build().create(ApiService.class);
    }

    static ApiConnector getInstance() {
        return instance;
    }

    public Observable<List<ItemSongResponse>> getMusic(String name){
        String newName = name.replaceAll(" ", "+");

        return Observable.create(new ObservableOnSubscribe<List<ItemSongResponse>>() {
            @Override
            public void subscribe(ObservableEmitter<List<ItemSongResponse>> e) throws Exception {
                List<ItemSongResponse> responses = new ArrayList<>();

                Document document = Jsoup.connect("http://mp3.zing.vn/tim-kiem/bai-hat.html?q=" + newName).get();

                Element element = document.select("div.wrap-content").first();

                Elements elements = element.select("div.item-song");

                for (Element element1 : elements){
                    String dataCode = element1.attr("data-code").toString();
                    String link = element1.select("h3").select("a").first().attr("href");
                    String title = element1.select("h3").select("a").first().attr("title");

                    responses.add(new ItemSongResponse(dataCode, title));
                }

                e.onNext(responses);
            }
        });
    }

    public Observable<List<ItemSongSearch>> getListMusic(String nameMusic, String webSite, String dataCode){
        return service.queryMusic(nameMusic, webSite, dataCode);
    }
}
