package com.vmodev.demoretrofit.interact;

import com.vmodev.demoretrofit.common.Constacts;
import com.vmodev.demoretrofit.module.ItemSongResponse;
import com.vmodev.demoretrofit.module.ItemSongSearch;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by dungtx on 7/24/17.
 */

public class AcountInteractor {
    private static AcountInteractor intance = new AcountInteractor();

    public AcountInteractor() {

    }

    public static AcountInteractor getIntance(){
        return intance;
    }

    public Observable<List<ItemSongResponse>> getMusic(String nameMusic){
        return ApiConnector.getInstance().getMusic(nameMusic);
    }

    public Observable<List<ItemSongSearch>> getListMusic(String nameMusic){
        return ApiConnector.getInstance().getListMusic(nameMusic, Constacts.WEB_SIZE_NAME, Constacts.CODE);
    }
}
