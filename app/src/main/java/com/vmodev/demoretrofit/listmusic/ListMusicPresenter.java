package com.vmodev.demoretrofit.listmusic;

import android.view.View;

import com.vmodev.demoretrofit.interact.AcountInteractor;
import com.vmodev.demoretrofit.module.ItemSongResponse;
import com.vmodev.demoretrofit.ui.base.Action;
import com.vmodev.demoretrofit.ui.base.BasePresenter;

import java.util.List;

/**
 * Created by dungtx on 7/24/17.
 */

public class ListMusicPresenter extends BasePresenter<IListMusic.View> implements IListMusic.Presenter {
    public ListMusicPresenter(IListMusic.View view) {
        super(view);
    }

    @Override
    public void getMusic(String name) {
        interact(AcountInteractor.getIntance().getMusic(name),
                new Action<List<ItemSongResponse>>() {
                    @Override
                    public void call(List<ItemSongResponse> itemSongReponses) {
                        mView.finishGetMusic(itemSongReponses);
                    }
                }, new Action<Throwable>() {
                    @Override
                    public void call(Throwable error) {
                        mView.errorGetMusic(error);
                    }
                });
    }

    @Override
    public void getItemSongSearch(String nameMusic) {
        interact(AcountInteractor.getIntance().getListMusic(nameMusic),
                response->{
                    mView.finishGetListMusic(response);
                }, error->{
                    mView.erroGetMusicSearch(error);
                });
    }
}
