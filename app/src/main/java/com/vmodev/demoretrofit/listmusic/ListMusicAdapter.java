package com.vmodev.demoretrofit.listmusic;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vmodev.demoretrofit.R;
import com.vmodev.demoretrofit.module.ItemSongSearch;

/**
 * Created by dungtx on 7/24/17.
 */

public class ListMusicAdapter extends RecyclerView.Adapter<ListMusicAdapter.ViewHolderListMusic> {

    private IListMusicAdapter mInterf;

    public ListMusicAdapter(IListMusicAdapter interf) {
        mInterf = interf;
    }

    @Override
    public ViewHolderListMusic onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_list_music_search, parent, false);
        return new ViewHolderListMusic(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderListMusic holder, int position) {
        ItemSongSearch itemSongSearch = mInterf.getData(position);
        holder.tvName.setText(itemSongSearch.getTitle());
        holder.tvArtist.setText(itemSongSearch.getArtist());

        if (itemSongSearch.getAvatar() == null || itemSongSearch.getAvatar().isEmpty()){
            return;
        }

        Picasso.with(holder.ivAvatar.getContext()).load(itemSongSearch.getAvatar())
                .placeholder(R.drawable.gb_loading)
                .error(R.drawable.gb_error)
                .into(holder.ivAvatar);

    }

    @Override
    public int getItemCount() {
        return mInterf.getCount();
    }

    class ViewHolderListMusic extends RecyclerView.ViewHolder {

        private TextView tvName;
        private ImageView ivAvatar;
        private TextView tvArtist;

        public ViewHolderListMusic(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvArtist = (TextView) itemView.findViewById(R.id.tv_artist);
            ivAvatar = (ImageView) itemView.findViewById(R.id.iv_avatar);

        }

    }

    public interface IListMusicAdapter{
        int getCount();
        ItemSongSearch getData(int position);

    }
}
