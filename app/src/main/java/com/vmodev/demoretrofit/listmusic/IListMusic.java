package com.vmodev.demoretrofit.listmusic;

import com.vmodev.demoretrofit.module.ItemSongResponse;
import com.vmodev.demoretrofit.module.ItemSongSearch;
import com.vmodev.demoretrofit.ui.base.IBasePresenter;
import com.vmodev.demoretrofit.ui.base.IViewMain;

import java.util.List;

/**
 * Created by dungtx on 7/24/17.
 */

public interface IListMusic {

    interface View extends IViewMain {

        void finishGetMusic(List<ItemSongResponse> itemSongReponses);

        void errorGetMusic(Throwable error);

        void finishGetListMusic(List<ItemSongSearch> response);

        void erroGetMusicSearch(Throwable error);
    }

    interface Presenter extends IBasePresenter {

        void getMusic(String s);

        void getItemSongSearch(String nameMusic);
    }
}
