package com.vmodev.demoretrofit.listmusic;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.vmodev.demoretrofit.R;
import com.vmodev.demoretrofit.module.ItemSongResponse;
import com.vmodev.demoretrofit.module.ItemSongSearch;
import com.vmodev.demoretrofit.ui.base.animation.ScreenAnimation;
import com.vmodev.demoretrofit.ui.base.fragment.BaseFragment;
import com.vmodev.demoretrofit.ui.base.fragment.BaseMvpFragment;
import com.vmodev.demoretrofit.ui.main.MainActivity;

import java.util.List;

/**
 * Created by dungtx on 7/24/17.
 */

public class ListMusicFragment extends BaseMvpFragment<IListMusic.Presenter>
        implements IListMusic.View, TextWatcher, ListMusicAdapter.IListMusicAdapter {
    private RecyclerView rcMusic;
    private EditText edtSearch;
    private ListMusicAdapter adapter;
    private List<ItemSongSearch> itemSongSearches;

    @Override
    public int getLayoutMain() {
        return R.layout.fragment_list_music;
    }

    @Override
    public void findViewByIds() {
        rcMusic = (RecyclerView) getView().findViewById(R.id.rc_music);
        edtSearch = (EditText) getView().findViewById(R.id.edt_search);

        adapter = new ListMusicAdapter(this);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        rcMusic.setLayoutManager(manager);
        rcMusic.setAdapter(adapter);
    }

    @Override
    public void initComponents() {
        mPresenter = new ListMusicPresenter(this);
    }

    @Override
    public void setEvents() {
        edtSearch.addTextChangedListener(this);
    }

    @Override
    public void finishGetMusic(List<ItemSongResponse> itemSongReponses) {
        Log.d("TAG", "...finish");
    }

    @Override
    public void errorGetMusic(Throwable error) {
        Log.d("TAG error", "....error");
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String content = s.toString().trim();
        mPresenter.onDestroy();
        mPresenter.getItemSongSearch(content);
    }

    @Override
    public void finishGetListMusic(List<ItemSongSearch> response) {
        Log.d("Finish getListMusic", "...finish");
        itemSongSearches = response;
        adapter.notifyDataSetChanged();
    }

    @Override
    public void erroGetMusicSearch(Throwable error) {
        Log.d("Error getListMusic", "...error");
    }

    @Override
    public int getCount() {
        if (itemSongSearches == null) {
            return 0;
        }
        return itemSongSearches.size();
    }

    @Override
    public ItemSongSearch getData(int position) {
        return itemSongSearches.get(position);
    }

}
