package com.vmodev.demoretrofit.ui.main;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.vmodev.demoretrofit.R;
import com.vmodev.demoretrofit.listmusic.ListMusicAdapter;
import com.vmodev.demoretrofit.listmusic.ListMusicFragment;
import com.vmodev.demoretrofit.module.ItemSongSearch;
import com.vmodev.demoretrofit.ui.base.activity.BaseActivity;
import com.vmodev.demoretrofit.ui.base.animation.ScreenAnimation;
import com.vmodev.demoretrofit.ui.base.fragment.BaseFragment;

import java.util.List;

public class MainActivity extends BaseActivity {

    @Override
    public int getLayoutMain() {
        return R.layout.activity_main;
    }

    @Override
    public void findViewByIds() {

    }

    @Override
    public void initComponents() {
        FragmentManager manager = getSupportFragmentManager();
        BaseFragment.
                openFragment(manager,
                        manager.beginTransaction(),
                        ListMusicFragment.class,
                        ScreenAnimation.NONE, null, false, true);
    }

    @Override
    public void setEvents() {

    }

}
