package com.vmodev.demoretrofit.ui.base.fragment;

import com.vmodev.demoretrofit.ui.base.IBasePresenter;

/**
 * Created by dungtx on 7/24/17.
 */

public abstract class BaseMvpFragment<P extends IBasePresenter> extends BaseFragment {

    protected P mPresenter;

    @Override
    public void onDestroyView() {
        if ( mPresenter != null ) {
            mPresenter.onDestroy();
        }
        super.onDestroyView();
    }
}
