package com.vmodev.demoretrofit.ui.base;

/**
 * Created by dungtx on 7/24/17.
 */

public interface Action<E> {
    void call(E e);
}
